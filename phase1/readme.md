# Pruebas unitarias con PHPunit

Para poder tener una prueba unitaria es importante saber que
debemos instalar el paquete de phunit

```sh
composer require --dev phpunit/phpunit
```

Una vez instalado el paquete podemos hacer nuestra primera prueba
unitaria, teniendo en cuenta que debemos de tener una dependencia 
de TestCase y extender de la misma

```php
<?php
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
```

Para poder ejecutar la prueba unitaria se puede hacer desde consola con:

```sh

phpunit ruta/de/la/prueba/UnitTest

phpunit ruta/

phpunit ruta/ --filter=testReturnsFullName // con esta ultima se puede ejecutar el text especifico dentro de una carpeta que contiene muchos tests

phpunit tests/ --color
```

Teniendo una salida paracida a la siguiente

```sh
PHPUnit 8.5.2 by Sebastian Bergmann and contributors.

Warning:       Invocation with class name is deprecated

.                                                                   1 / 1 (100%)

Time: 15 ms, Memory: 4.00 MB

OK (1 test, 5 assertions)
```

Recordar que para hacer comparaciones de datos es importante saber de las 
[aserciones](https://phpunit.readthedocs.io/es/latest/assertions.html#appendixes-assertions)

[Archivo de configuracion](https://phpunit.readthedocs.io/es/latest/configuration.html)


Para este caso practica se cuenta con una carpeta llamada tests