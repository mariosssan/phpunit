<?php

use PHPUnit\Framework\TestCase;

class QueueTest03 extends TestCase
{
    protected static $queue;
    
    protected function setUp(): void
    {
        echo "Se ejecuta cada vez que se neceta un test nuevo\n";
        static::$queue->clear();      
    }
    
    public static function setUpBeforeClass(): void
    {
        static::$queue = new Queue;        
    }
    
    public static function tearDownAfterClass(): void
    {
        static::$queue = null;        
    }    
        
    public function testNewQueueIsEmpty()
    {
        $this->assertEquals(0, static::$queue->getCount());
    }

    public function testAnItemIsAddedToTheQueue()
    {
        static::$queue->push('green');

        var_dump(static::$queue);
        
        $this->assertEquals(1, static::$queue->getCount());
    }

    public function testAnItemIsRemovedFromTheQueue()
    {
        static::$queue->push('green');
        var_dump(static::$queue);

                
        $item = static::$queue->pop();
        
        $this->assertEquals(0, static::$queue->getCount());

        $this->assertEquals('green', $item);
    }
    
    public function testAnItemIsRemovedFromTheFrontOfTheQueue()
    {
        static::$queue->push('first');
        static::$queue->push('second');

        var_dump(static::$queue);
        
        $this->assertEquals('first', static::$queue->pop());
    }    
}
